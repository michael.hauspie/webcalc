class Calculator:
    def do_op(self, operation):
        """Effectue le calcul décrit dans operation et retourne le résultat.
        Le calcul est décrit sous la forme d'un dictionnaire contenant 3 clés:
        - operation: une chaine de caractère qui décrit l'opération à effectuer
        - op1: le premier opérande de l'opération
        - op2: le deuxième opérande de l'opération

        le resultat est retourné sous la forme d'un dictionnaire de deux valeurs telles que:
        - operation: l'operation sous la forme mathématique usuelle. Par exemple l'operation PLUS 3 4 donnera "3 + 4" dans ce champ
        - resultat: le résultat de l'operation sous la forme d'un nombre

        Retourne None si le calcul est impossible (opération inconnue, division par 0...)
        """
        op1 = operation['op1']
        op2 = operation['op2']
        operation = operation['operation']
        if operation == 'PLUS':
            return {
                'operation': '{} + {}'.format(op1, op2),
                'resultat': op1 + op2,
            }
        elif operation == 'FOIS':
            return {
                'operation': '{} * {}'.format(op1, op2),
                'resultat': op1 * op2,
            }
        elif operation == 'DIVISE':
            return {
                'operation': '{} / {}'.format(op1, op2),
                'resultat': op1 / op2,
            }
        return None
            
        
