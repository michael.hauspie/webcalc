#!/usr/bin/env python3

# A simple calculator web application


from http.server import HTTPServer, BaseHTTPRequestHandler
from io import BytesIO

import json
import calc

class CalculatorRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        self.wfile.write("""
Cette application permet d'executer des calculs simple. 

Pour l'utiliser, il faut envoyer le calcul à effectuer sous la forme d'un objet json dans une requete POST sur '/'

Les données doivent contenir trois valeur:

- operation (chaine de caractères): à choisir parmis les opérations possibles: "PLUS", "MOINS", "FOIS", "DIVISE"
- op1 (nombre): le premier operande
- op2 (nombre): le deuxième opérande

Exemple:

{
   "operation": "PLUS",
   "op1": 3,
   "op2": 4,
}

L'application retourne un résultat au format json avec le résultat (nombre) et l'opération effectuée (chaîne)

Exemple:

{
   "operation": "3 + 4",
   "resultat": 7,
}

Pour envoyer avec curl:

curl -X POST http://monurl -H 'Content-Type: application/json' -d '{"operation": "PLUS", "op1": 3, "op2": 4}'

""".encode())

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        body = json.loads(self.rfile.read(content_length))
        calculator = calc.Calculator()
        reponse = calculator.do_op(body)
        if reponse == None:
            self.send_response(500)
            self.end_headers()
            return None
        self.send_response(200)
        self.end_headers()
        self.wfile.write(json.dumps(reponse).encode())



        
httpd = HTTPServer(('0.0.0.0', 8000), CalculatorRequestHandler)
httpd.serve_forever()


