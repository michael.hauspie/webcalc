#!/usr/bin/env python3


import calc
import unittest
import random

class TestCalculator(unittest.TestCase):
    def test_PLUS(self):
        calculator = calc.Calculator()
        operation = {
            'operation': 'PLUS',
            'op1': random.randint(-20,20),
            'op2': random.randint(-20,20)
        }
        result = calculator.do_op(operation)
        self.assertEqual(result['operation'], "{} + {}".format(operation['op1'], operation['op2']))
        self.assertEqual(result['resultat'], operation['op1'] + operation['op2'])

    def test_DIVISE(self):
        calculator = calc.Calculator()
        operation = {
            'operation': 'DIVISE',
            'op1': random.randint(-20,20),
            'op2': random.randint(-20,20)
        }
        result = calculator.do_op(operation)
        self.assertEqual(result['operation'], "{} / {}".format(operation['op1'], operation['op2']))
        self.assertEqual(result['resultat'], operation['op1'] / operation['op2'])


    def test_FOIS(self):
        calculator = calc.Calculator()
        operation = {
            'operation': 'FOIS',
            'op1': random.randint(-20,20),
            'op2': random.randint(-20,20)
        }
        result = calculator.do_op(operation)
        self.assertEqual(result['operation'], "{} * {}".format(operation['op1'], operation['op2']))
        self.assertEqual(result['resultat'], operation['op1'] * operation['op2'])

    def test_INCONNUE(self):
        calculator = calc.Calculator()
        operation = {
            'operation': 'INCONNUE',
            'op1': random.randint(-20,20),
            'op2': random.randint(-20,20)
        }
        result = calculator.do_op(operation)
        self.assertEqual(result, None)
        

if __name__ == '__main__':
    unittest.main()
