# Une calcultrice python simple

Une calculatrice accessible via une requete HTTP permettant d'exécuter
des calculs simples.

Pour la lancer

```shell
/usr/bin/env python3 main.py
```

La calculatrice crée alors un serveur HTTP sur le port 8000. Les
instructions pour son utilisation peuvent être obtenue en effectuant
une requête `GET` sur la ressource `/`
